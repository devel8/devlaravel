<?php

namespace Database\Factories;

use App\Models\libro;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class libroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = libro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => $this->faker->unique()->sentence(8),
            'autor' => $this->faker->sentence(4),
            'editorial' => $this->faker->sentence(10),
            'anio' => $this->faker->randomNumber
        ];
    }
}
