<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Libro;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libros = Libro::orderBy('created_at','DESC')->paginate(5);
        return view('web.libro', compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('web.agregar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$data = request()->all();
        /*$data = request()->except('_token');
        Libro::insert($data);
        return response()->json($data);*/ //comment by testing
        $libro = new Libro();

        $libro->titulo = $request->get('title');
        $libro->autor = $request->get('author');
        $libro->editorial = $request->get('editor');
        $libro->anio = $request->get('anio');
        $libro->save();

        return redirect('libro');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libro = Libro::where('_id', $id)->first();
        return view('web.editar', compact('libro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = request()->except('_token','_method');
        $libro = Libro::find($id);

        $libro->titulo = $data['titleupd'];
        $libro->autor = $data['authorupd'];
        $libro->editorial = $data['editorupd'];
        $libro->anio = $data['anioupd'];
        $libro->save();

        return redirect('libro');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Libro::destroy($id);
        return redirect('libro');
    }
}
