<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;

class Libro extends MongoModel
{
    use HasFactory;

    protected $primaryKey = '_id';
    protected $fillable = [
        'titulo',
        'autor',
        'editorial',
        'anio',
    ];
}
