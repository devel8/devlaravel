<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::redirect('/', 'libro');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('libro', 'App\Http\Controllers\web\LibroController@index')->name('libro');
Route::post('save', 'App\Http\Controllers\web\LibroController@store')->name('guardar');
Route::get('add', 'App\Http\Controllers\web\LibroController@create')->name('agregar');
Route::get('libro/{id}', 'App\Http\Controllers\web\LibroController@show')->name('ver');
Route::put('update/{id}', 'App\Http\Controllers\web\LibroController@update')->name('actualizar');
Route::get('delete/{id}', 'App\Http\Controllers\web\LibroController@destroy')->name('eliminar');

require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
